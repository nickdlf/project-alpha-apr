from django.contrib import admin
from images.models import Image

# Register your models here.
@admin.register(Image)
class Image (admin.ModelAdmin):
    list_display = (
        "phrase",
        "ai_image",
    )