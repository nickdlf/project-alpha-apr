from django.db import models

# Create your models here.


class Image(models.Model):
    phrase = models.TextField()
    ai_image = models.ImageField(upload_to='images/')
    prompt = models.ForeignKey('projects.Prompt', on_delete=models.CASCADE, related_name='images', null=True)    # Other fields...

    def __str__(self):
        return str(self.phrase)
