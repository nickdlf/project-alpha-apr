from django import template
import random

register = template.Library()

@register.simple_tag
def random_number(max_val):
    return random.randint(1, max_val)