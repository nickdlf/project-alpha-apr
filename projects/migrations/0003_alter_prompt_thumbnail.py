# Generated by Django 4.2.1 on 2023-06-11 02:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        (
            "images",
            "0002_image_prompt_alter_image_ai_image_alter_image_phrase",
        ),
        ("projects", "0002_category_prompt"),
    ]

    operations = [
        migrations.AlterField(
            model_name="prompt",
            name="thumbnail",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="images.image",
            ),
        ),
    ]
