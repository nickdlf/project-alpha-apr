from django.contrib import admin
from projects.models import Project, Prompt, Category


# Register your models here.


@admin.register(Project)
class Project(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
    )



@admin.register(Prompt)
class Prompt(admin.ModelAdmin):
    list_display = (
        "text",
        "created_at",
        "user",
        "thumbnail",
        "category",
    )


@admin.register(Category)
class Category(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )



