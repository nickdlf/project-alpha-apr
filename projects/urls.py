from django.urls import path
from projects.views import (list_projects, show_project, create_project, chatbot, generate_image_from_txt, show_prompt, new_category)



urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
    path("chat/", chatbot, name="chatbot"),
    path("images/", generate_image_from_txt, name="generate_image"),
    path('prompt/<int:id>/', show_prompt, name='show_prompt'),
    path('category/', new_category, name='create_category')
]
