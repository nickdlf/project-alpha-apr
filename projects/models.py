from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from images.models import Image

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name

class Category(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)  # assuming you are using Django's default User model
    
    def __str__(self):
        return self.name


class Prompt(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    thumbnail = models.ForeignKey('images.Image', null=True, blank=True, on_delete=models.SET_NULL, related_name='+') 
    category = models.ForeignKey(Category, null=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        return self.text