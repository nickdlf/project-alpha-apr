from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm, CategoryForm
import openai, os
from dotenv import load_dotenv

load_dotenv()
from django.conf import settings

api_key = os.getenv("OPENAI_KEY", None)
from openai.error import RateLimitError
from nltk.tokenize import sent_tokenize  # you might need to install nltk
import nltk
from nltk.tokenize import sent_tokenize

nltk.download("punkt")  # ensures the punkt tokenizer is downloaded
import requests
import shutil
import requests
from django.core.files.base import ContentFile
from images.models import Image
import re
from projects.models import Prompt, Category, Image


# Create your views here.
@login_required
def list_projects(request):
    category_id = request.GET.get("category")
    projects = Project.objects.filter(owner=request.user)
    prompts = Prompt.objects.all().order_by("-created_at")
    categories = Category.objects.all()

    if category_id:
        category = Category.objects.get(id=category_id)
        prompts = prompts.filter(category=category)

    context = {
        "projects": projects,
        "prompts": prompts,
        "categories": categories,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {
        "project": project,
    }
    return render(request, "projects/detail.html", context)


@login_required
def show_prompt(request, id):
    try:
        prompt = Prompt.objects.get(id=id)
        images = prompt.images.all()
        if images:
            print("Images exist for this prompt")
        else:
            print("No images for this prompt")

        # Fetch the next and previous prompts based on their IDs.
        next_prompt = Prompt.objects.filter(id__gt=id).order_by("id").first()
        previous_prompt = (
            Prompt.objects.filter(id__lt=id).order_by("-id").first()
        )

        instruction_image_pairs = [(image.phrase, image) for image in images]
    except:
        return redirect("list_projects")

    context = {
        "prompt": prompt,
        "instruction_image_pairs": instruction_image_pairs,
        # Add next_prompt and previous_prompt to the context.
        "next_prompt": next_prompt,
        "previous_prompt": previous_prompt,
    }
    return render(request, "projects/prompt.html", context)


@login_required
def new_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("list_projects")
    else:
        form = CategoryForm()

    categories = Category.objects.all()
    context = {
        "category_form": form,
        "categories": categories,
    }
    return render(request, "projects/create_category.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


import re


def chatbot(request):
    # Query the categories at the beginning of the view
    categories = Category.objects.all()
    instruction_image_pairs = []
    if api_key is not None and request.method == "POST":
        openai.api_key = api_key
        user_input = request.POST.get("user_input")
        prompt = f"In five steps or less teach me how to: {user_input}"
        first_image = None

        category_id = request.POST.get("category")
        category = Category.objects.get(id=category_id)

        new_prompt = Prompt(text=prompt, user=request.user, category=category)
        new_prompt.save()

        obj = None

        try:
            response = openai.Completion.create(
                engine="text-davinci-003",
                prompt=prompt,
                max_tokens=256,
                temperature=0.5,
            )

            chatbot_response = response["choices"][0]["text"]

            # Split the response into sentences
            chatbot_response_list = sent_tokenize(chatbot_response)

            for sentence in chatbot_response_list:
                # Remove step number from the sentence
                sentence_without_number = re.sub(
                    r"^\D*\d+[\.:]*\s*", "", sentence
                )

                if not first_image and obj:
                    first_image = obj

                if (
                    sentence_without_number.strip()
                ):  # Check if the sentence is not empty
                    img_response = openai.Image.create(
                        prompt=sentence_without_number,
                        size="256x256",
                    )

                    img_url = img_response["data"][0]["url"]
                    img_response = requests.get(img_url)
                    img_file = ContentFile(img_response.content)

                    count = Image.objects.count() + 1
                    fname = f"image-{count}.jpg"

                    obj = Image(
                        phrase=sentence_without_number, prompt=new_prompt
                    )
                    obj.ai_image.save(fname, img_file)
                    obj.save()

                    # Create pairs of instructions and images
                    instruction_image_pairs.append(
                        (sentence_without_number, obj)
                    )

            if first_image is None and obj:
                first_image = obj

            new_prompt.thumbnail = first_image
            new_prompt.save()
        except RateLimitError:
            chatbot_response = "We're sorry, but our chatbot has exceeded its quota. Please try again later."

    # We update the context here so it contains the categories irrespective of whether it's a GET or POST request.
    context = {
        "instruction_image_pairs": instruction_image_pairs,
        "categories": categories,
    }

    return render(request, "projects/chat.html", context)


# def list_prompts(request):
#     category_id = request.GET.get('category')
#     if category_id:
#         prompts = Prompt.objects.filter(category__id=category_id).order_by('-created_at')
#     else:
#         prompts = Prompt.objects.all().order_by('-created_at')

#     categories = Category.objects.all()

#     return render(request, "projects/list.html", {"prompts": prompts, "categories": categories})


# def list_prompts(request):
#     prompts = Prompt.objects.all().order_by('-created_at')
#     return render(request, "list.html", {"prompts": prompts})

# def chatbot(request):
#     chatbot_response = None
#     context = {}

#     if api_key is not None and request.method == "POST":
#         openai.api_key = api_key
#         user_input = request.POST.get('user_input')
#         prompt = user_input

#         try:
#             response = openai.Completion.create(
#                 engine = 'text-davinci-003',
#                 prompt = prompt,
#                 max_tokens = 256,
#                 temperature = 0.5,
#             )

#             chatbot_response = response["choices"][0]["text"]

#             img_response = openai.Image.create(
#             prompt = chatbot_response,
#             size = '256x256',
#             )

#             img_url = response["data"][0]["url"]

#             response = requests.get(img_url)
#             img_file = ContentFile(response.content)

#             count = Image.objects.count() + 1
#             fname = f"image-{count}.jpg"

#             obj = Image(phrase=user_input)
#             obj.ai_image.save(fname, img_file)
#             obj.save()
#             print(obj)

#         except RateLimitError:
#             chatbot_response = "We're sorry, but our chatbot has exceeded its quota. Please try again later."

#         # Split the response into sentences
#         chatbot_response_list = sent_tokenize(chatbot_response)


#         context = {
#             "response_list": chatbot_response_list,
#             "object": obj,
#             }

#     return render(request, "projects/chat.html", context)


# "dalle_images": save_image(dalle, f"{self.title}.jpg"),

# dalle = openai.Image.create(
#     prompt = response,
#     n=1,
#     size='512x512',
# )

# @staticmethod
# def save_image(image_url, file_name):
#     image_res = requests.get(image_url, stream = True)

#     if image_res.status_code == 200:
#         with open(file_name,'wb') as f:
#             shutil.copyfileobj(image_res.raw, f)
#     else:
#         print("Error downloading image!")
#     return image_res.status_code


api_key = os.getenv("OPENAI_KEY", None)
openai.api_key = api_key


def generate_image_from_txt(request):
    obj = None
    if api_key is not None and request.method == "POST":
        user_input = request.POST.get("user_input")
        response = openai.Image.create(
            prompt=(f"in five steps or less teach me how to {user_input}"),
            size="256x256",
        )

        img_url = response["data"][0]["url"]

        response = requests.get(img_url)
        img_file = ContentFile(response.content)

        count = Image.objects.count() + 1
        fname = f"image-{count}.jpg"

        obj = Image(phrase=user_input)
        obj.ai_image.save(fname, img_file)
        obj.save()

        print(obj)

    return render(request, "projects/image.html", {"object": obj})
